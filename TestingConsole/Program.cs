﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestingConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (var item in CheckYield(1,50, 10))
            {
                Console.WriteLine(item);
                Console.WriteLine("sleeping...");
                Thread.Sleep(1000);

            }
            Console.WriteLine("first 50 done");
            foreach (var item in CheckYield(50,100, 10))
            {
                Console.WriteLine(item);
                Console.WriteLine("sleeping...");
                Thread.Sleep(1000);

            }
            Console.ReadLine();
            //testing dev local
            //testing merge
        }

        public static IEnumerable<int> CheckYield(int start, int limit, int batchSize)
        {
            for (int i = start;  i < limit; i++)
            {
                if (i % batchSize == 0)
                    yield return i;
            }
        }
    }
}
